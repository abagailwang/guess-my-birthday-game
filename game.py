from random import randint

#Ask the human for their name
name = input("Hi! What is your name?")

#Print the guess
for guess_number in range (1, 6):
    month = randint(1,12)
    year = randint(1924,2004)

    print("Guess", guess_number, name, "Were you born in ",month,"/",year, "?")
    answer = input("yes or no?")

    if answer == "yes":
        print("I knew it!")
        break

    elif guess_number == 5:
        print("I Have other things to do. Goodbye.")

    else:
        print("Drat! Let me try again!")
